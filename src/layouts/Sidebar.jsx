/* eslint-disable no-lone-blocks */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import Collapse from '@material-ui/core/Collapse';

import { Link } from 'react-router-dom';
import routes from '../routes';
const drawerWidth = 240;

const styles = theme => ({
  root:{
    color: '#fff',
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  listItemText: {
    fontWeight: 'bold',
    fontSize: '18px'
  },
    drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: '0px',
    [theme.breakpoints.up('sm')]: {
      width: '0px'
    },
  },
  drawerPaper: {
    backgroundImage: 'linear-gradient(120deg, #1D5B92 0%, #6ED3F2 100%)',
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  link: {
    textDecoration: 'none'
  },
  list: {
    marginLeft: '25px',
    marginRight: '25px'
  },
  listItem: {
    color: '#fff !important',
    fontWeight: 'bold',
    fontSize: '18px'
  },
  logo: {
    width: '100%'
  },
  collapse:{
    paddingLeft: '25px'
  }
});

class Sidebar extends React.Component {
  state = {
    programation: false,
    billing: false,
    reports: false,
    users: false,
    vehicles: false,
    open: true
  };

  componentDidUpdate(prevProps, prevState){
    if (prevProps.state && !prevState.open) {

      this.setState({ open: true });
      this.props.onSideCollapse(this.state.open)
    }

  }
  componentWillMount(){
    const route =  window.location.pathname.split('/');
    if(route.includes('programation')){
      this.setState({ programation: true });
      if(route.includes('users')){
        this.setState({ users: true });
      }
      if(route.includes('vehicles')){
        this.setState({ vehicles: true });
      }
    }
    if(route.includes('reports')){
      this.setState({ reports: true });

    }
    if(route.includes('billing')){
      this.setState({ billing: true });

    }
  }
  handleDrawerOpen = () => {
    const state = this.props.state;
    this.setState({ open: state });

  };

  handleDrawerClose = () => {

    this.setState({ open: false });
    this.props.onSideCollapse(false);
  };

  renderChilds(pageName, routes){
    const { classes } = this.props;

    const childs = routes.filter((route)=>{
      return route.childOf === pageName;
    });

    return(
      childs.map((child, index) => {
        {
          if (pageName === 'Programación' && child.pageName === 'Usuarios') {
            return(
                <div className="listCollapse">
                    <ListItem button onClick={this.handleUserClick} style={{marginTop: '15px', marginBottom: '15px'}}>
                      <ListItemText  primary={<Typography variant="p" className={classes.listItem}>{child.pageName}</Typography>} />
                      {this.state.users ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>

                  <Collapse in={this.state.users} timeout="auto"  unmountOnExit>
                      <List component="div" className={classes.collapse}>
                        <Link to={'/programation/users/load'} key={index} className={classes.link}>
                          <ListItem button style={{marginTop: '5px', marginBottom: '5px'}}>
                            <ListItemText primary={<Typography variant="p" style={{  whiteSpace: 'pre-line'}}>{'Cargar'}</Typography>} className={classes.listItemText}/>
                          </ListItem>
                        </Link>
                        <Link to={'/programation/users/see'} key={index+1} className={classes.link}>
                          <ListItem button style={{marginTop: '5px', marginBottom: '5px'}}>
                            <ListItemText primary={<Typography variant="p" style={{  whiteSpace: 'pre-line'}}>{'Ver'}</Typography>} className={classes.listItemText}/>
                          </ListItem>
                        </Link>
                      </List>
                  </Collapse>
                </div>
              );
          } else if(pageName === 'Programación' && child.pageName === 'Vehiculos') {
            return(
              <div className="listCollapse">
              <ListItem button onClick={this.handleVehiclesClick} style={{marginTop: '15px', marginBottom: '15px'}}>
                <ListItemText  primary={<Typography variant="p" className={classes.listItem}>{child.pageName}</Typography>} />
                {this.state.vehicles ? <ExpandLess /> : <ExpandMore />}
              </ListItem>

            <Collapse in={this.state.vehicles} timeout="auto"  unmountOnExit>
                <List component="div" className={classes.collapse}>
                  <Link to={'/programation/vehicles/load'} key={30} className={classes.link}>
                    <ListItem button style={{marginTop: '5px', marginBottom: '5px'}}>
                      <ListItemText primary={<Typography variant="p" style={{  whiteSpace: 'pre-line'}}>{'Cargar'}</Typography>} className={classes.listItemText}/>
                    </ListItem>
                  </Link>
                  <Link to={'/programation/vehicles/see'} key={31} className={classes.link}>
                    <ListItem button style={{marginTop: '5px', marginBottom: '5px'}}>
                      <ListItemText primary={<Typography variant="p" style={{  whiteSpace: 'pre-line'}}>{'Ver'}</Typography>} className={classes.listItemText}/>
                    </ListItem>
                  </Link>
                </List>
            </Collapse>
          </div>
            );
          } else {
            return(
              <Link to={child.path} key={index} className={classes.link}>
                <ListItem button style={{marginTop: '15px', marginBottom: '15px'}}>
                  <ListItemText primary={<Typography variant="p" style={{  whiteSpace: 'pre-line'}}>{child.pageName}</Typography>} className={classes.listItemText}/>
                </ListItem>
              </Link>
            );
          }
          
        }
        
      }) 
    ); 
  }

  handleClick = (e)=>{
    const element = e.currentTarget;
    const route = element.querySelector('span').innerText
    if (route === 'Programación') {
      this.setState(state => ({ programation: !state.programation }));   
    }
    if (route === 'Facturación') {
      this.setState(state => ({ billing: !state.billing }));   
    }
    if (route === 'Reportes') {
      this.setState(state => ({ reports: !state.reports }));   
    }
  }
  handleUserClick = () => {
    this.setState(state => ({ users: !state.users }));   

  }
  handleVehiclesClick = () => {
    this.setState(state => ({ vehicles: !state.vehicles }));   

  }
  render() {
    const { classes } = this.props;

    return (
        <Drawer
          variant="permanent"
          open={this.state.open}
          className={classNames(classes.drawer, {
            [classes.drawerOpen]: this.state.open,
            [classes.drawerClose]: !this.state.open,
          })}
          classes={{
            paper: classNames(classes.drawerPaper, {
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open,
            }),
          }}
        >
          <img src="/images/logo.png" alt="" className={classes.logo}/>
          <List className={classes.list}>
          <ListItem button onClick={this.handleDrawerClose} style={{marginTop: '15px', marginBottom: '15px'}}>
            <ListItemText primary={<Typography variant="p" style={{color: '#fff',fontWeight: 'bold',
    fontSize: '18px'}}>
            {this.state.open ?
              <span className="menuText">{`<< Ocultar Menu`}</span>
            : <span  onClick={this.handleDrawerOpen}> >> </span>
            }</Typography>} />
          </ListItem>
          {routes.map((route, index, self) => {
            if (route.show && route.childOf === undefined) {
              if (route.pageName === 'Programación') {
                return(
                  <div className="listCollapse">
                    <Link to={route.path} key={index} className={classes.link}>
                      <ListItem button onClick={this.handleClick} style={{marginTop: '15px', marginBottom: '15px'}}>
                        <ListItemText  primary={<Typography variant="p" className={classes.listItem}>{route.pageName}</Typography>} />
                        {this.state.programation ? <ExpandLess /> : <ExpandMore />}
                      </ListItem>
                    </Link>
                    <Collapse in={this.state.programation} timeout="auto" unmountOnExit>
                      <List component="div" className={classes.collapse}>
                        {this.renderChilds(route.pageName, self)}
                      </List>
                    </Collapse>
                  </div>
                );
              }else if(route.pageName === 'Facturación') {
                return(
                  <div className="listCollapse">
                    <Link to={'/billing'} className={classes.link}>
                      <ListItem button onClick={this.handleClick} style={{marginTop: '15px', marginBottom: '15px'}}>
                        <ListItemText  primary={<Typography variant="p" className={classes.listItem}>{route.pageName}</Typography>} className={classes.listItemText.root}/>
                        {this.state.billing ? <ExpandLess /> : <ExpandMore />}
                      </ListItem>
                    </Link>
                    <Collapse in={this.state.billing} timeout="auto"  unmountOnExit>
                      <List component="div" className={classes.collapse}>
                        {this.renderChilds(route.pageName, self)}
                      </List>
                    </Collapse>
                  </div>
                );
              }else if(route.pageName === 'Reportes'){
                return(
                  <div className="listCollapse">
                    <Link to={'/reports'} className={classes.link}>
                      <ListItem button onClick={this.handleClick} style={{marginTop: '15px', marginBottom: '15px'}}>
                        <ListItemText  primary={<Typography variant="p" className={classes.listItem}>{route.pageName}</Typography>}/>
                        {this.state.reports ? <ExpandLess /> : <ExpandMore />}
                      </ListItem>
                    </Link>
                    <Collapse in={this.state.reports} timeout="auto"  unmountOnExit>
                      <List component="div" className={classes.collapse}>
                        {this.renderChilds(route.pageName, self)}
                      </List>
                    </Collapse>
                  </div>
                );
              }else{
                return (
                  <Link to={route.path} key={index} className={classes.link}>
                    <ListItem button style={{marginTop: '15px', marginBottom: '15px'}}>
                      <ListItemText primary={<Typography variant="p" className={classes.listItem}>{route.pageName}</Typography>} />
                    </ListItem>
                  </Link>
                  
                );
              }
            }
          })}
        </List>
        </Drawer>
        
    );
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Sidebar);