import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/KeyboardArrowRight';

import Grid from '@material-ui/core/Grid';
import Sidebar from './Sidebar';
import global from '../pages/global';
import muiTheme from './theme';
const drawerWidth = 240;

const mui = createMuiTheme(muiTheme);
const styles = theme => ({
  root: {
    display: 'flex',
    color: '#fff'
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  avatar:{
    color: global.mainTextColor,
    fontSize: '21px',
    fontWeight: 'bold',
    marginLeft: '15px'
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    backgroundImage: 'linear-gradient(120deg, #89f7fe 0%, #66a6ff 100%)',
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  top: {
    display: 'inline-flex',
    alignItems: 'flex-end'
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  chartContainer: {
    marginLeft: -22,
  },
  tableContainer: {
    height: 320,
  },
  h5: {
    marginBottom: theme.spacing.unit * 2,
  },
  sideIndicator:{
    height:'100%',
    backgroundColor: '#1D5B92'
  },
  avatarIcon:{
    width:'50px'
  }
});

class Dashboard extends React.Component {
  state = {
    open: true,
    sideState: true,
    sideMarker: false
  };
  constructor(props){
    super(props);
    this.sideStateChange = this.sideStateChange.bind(this);
    this.onSideCollapse = this.onSideCollapse.bind(this);
  }
  sideStateChange(){
    this.setState({ sideState: true });

  }
  onSideCollapse(state){
    this.setState({ sideMarker: !state });
  }
  render() {
    const { classes } = this.props;

    return (
      
      <div className={classes.root}>
        
        <MuiThemeProvider theme={mui}>
        <CssBaseline />
          <Sidebar state={this.state.sideState} onSideCollapse={this.onSideCollapse}/>
          {this.state.sideMarker &&
            <div className={classes.sideIndicator} onClick={this.sideStateChange}>  
              <MenuIcon />
            </div>

          }
          <main className={classes.content}>
          <Grid container spacing={this.props.spacing}>
            <Grid item xs={12}>
              <Grid
                container
                direction="row"
                justify="flex-end"
                alignItems="center"
              >
                <img src="/images/avatar.png" alt="" className={classes.avatarIcon}/>
                <Typography component="p" className={classes.avatar}>
                  Salir
                </Typography>
              </Grid>
            </Grid>
              {this.props.children}
            
          </Grid>
          </main>
        </MuiThemeProvider>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Dashboard);