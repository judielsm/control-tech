import Login from './pages/login';
import Summary from './pages/summary'
import Map from './pages/map'
import List from './pages/map/List'
import Programation from './pages/programation';
import Reports from './pages/reports';
import Billing from './pages/billing';

export default [
    {
        path: '/',
        component: Login,
        pageName: 'base',
        show: false,
        exact: true
    },
    {
        path: '/login',
        component: Login,
        pageName: 'Iniciar Sesión',
        show: false,
        exact: true
    },
    {
        path: '/summary',
        component: Summary,
        pageName: 'Resumen',
        show: true,
        exact: true
    },
    {
        path: '/map',
        component: Map.Map,
        show: true,
        pageName: 'Mapa',
        exact: true
    },
    {
        path: '/map/listing',
        component: List,
        show: false,
        pageName: 'Listado',
        exact: true
    },
    {
        path: '/programation',
        component: Programation.Programation,
        show: true,
        pageName: 'Programación',
        parent: true,
        exact: true
    },
    {
        path: '/programation/users',
        component: Programation.LoadUsers,
        show: true,
        pageName: 'Usuarios',
        childOf: 'Programación',
        exact: true
    },
    {
        path: '/programation/vehicles',
        component: Programation.LoadVehicles,
        show: true,
        childOf: 'Programación',
        pageName: 'Vehiculos',
        exact: true
    },
    {
        path: '/programation/users/load',
        component: Programation.LoadUsers,
        show: true,
        pageName: 'Cargar',
        childOf: 'Usuarios',
        exact: true
    },
    {
        path: '/programation/users/see',
        component: Programation.SeeUsers,
        show: true,
        childOf: 'Usuarios',
        pageName: 'Ver',
        exact: true
    },
    {
        path: '/programation/vehicles/load',
        component: Programation.LoadVehicles,
        show: true,
        pageName: 'Cargar',
        childOf: 'Vehiculos',
        exact: true
    },
    {
        path: '/programation/vehicles/see',
        component: Programation.SeeVehicles,
        show: true,
        pageName: 'Ver',
        childOf: 'Vehiculos',
        exact: true
    },
    {
        path: '/reports',
        component: Reports.Base,
        show: true,
        pageName: 'Reportes',
        exact: true
    },
    {
        path: '/reports/transported',
        component: Reports.Passengers,
        show: true,
        childOf:'Reportes',
        pageName: 'Pasajeros Transportados',
        exact: true
    },
    {
        path: '/reports/historical',
        component: Reports.Historical,
        show: true,
        childOf:'Reportes',
        pageName: 'Histórico de rutas',
        exact: true
    },
    {
        path: '/reports/summary',
        component: Reports.Summary,
        show: true,
        childOf:'Reportes',
        pageName: 'Resumen de operación',
        exact: true
    },
    {
        path: '/reports/gps',
        component: Reports.Gps,
        show: true,
        childOf:'Reportes',
        pageName: 'Reporte de GPS',
        exact: true
    },
    {
        path: '/reports/app',
        component: Reports.App,
        show: true,
        childOf:'Reportes',
        pageName: 'App de conductores',
        exact: true
    },
    {
        path: '/reports/users',
        component: Reports.Users,
        show: true,
        childOf:'Reportes',
        pageName: 'Pasajeros Transportados',
        exact: true
    },
    {
        path: '/reports/speed',
        component: Reports.Velocity,
        show: true,
        childOf:'Reportes',
        pageName: 'Reporte de Velocidad',
        exact: true
    },
    {
        path: '/billing',
        component: Billing.Base,
        show: true,
        pageName: 'Facturación',
        exact: true
    },
    {
        path: '/billing/parameters',
        component: Billing.Parameters,
        show: true,
        childOf: 'Facturación',

        pageName: 'Parámetros',
        exact: true
    },
    {
        path: '/billing/report',
        component: Billing.Report,
        show: true,
        childOf: 'Facturación',

        pageName: 'Reporte',
        exact: true
    },
];