import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import global from '../global';
const muiTheme = createMuiTheme({
  palette: {
    primary: { main: '#EC8153' },
    secondary: { main: '#1EB7E6' },
  },
  text: {
    primary: { main: 'rgb(255, 255, 255)' },

  }
});
const styles = theme => ({

  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  title: {
    color: global.mainTextColor,
    marginTop: '25px'
  },
  paper: {
    marginTop: '15vh',
    height: '400px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  registration: {
    marginTop: 'auto'
  },
  registrationText: {
    color: global.mainButtonColor
  },
  submit: {
    padding: '6px 30px',
    width: 'auto',
    display: 'flex',
    margin: '0 auto',
    marginTop: theme.spacing.unit * 3,
    backgroundColor: global.mainButtonColor,
    color: '#fff'
  },
});

class Login extends Component {
    constructor(props){
      super(props);
      this.ButtonClicked = this.ButtonClicked.bind(this);
    }
    onSubmit(){
      
    }
    ButtonClicked(){      
      this.props.history.push('/summary');

    }
    render() {
        return (
          <MuiThemeProvider theme={muiTheme}>

              <main className={this.props.classes.main}>
                
                <Paper className={this.props.classes.paper}>
                  <Typography component="h1" variant="h5" className={this.props.classes.title}>
                    Iniciar Sesión
                  </Typography>
                  <form className={this.props.classes.form} onSubmit={this.onSubmit}>
                    <FormControl margin="normal"  fullWidth>
                      <InputLabel htmlFor="name">Nombre</InputLabel>
                      <Input id="name" name="text" autoComplete="name" autoFocus />
                    </FormControl>
                    <FormControl margin="normal"  fullWidth>
                      <InputLabel htmlFor="password">Contraseña</InputLabel>
                      <Input name="password" type="password" id="password" autoComplete="current-password" />
                    </FormControl>
                    <FormControl margin="normal"  fullWidth>
                      <InputLabel htmlFor="country">Pais</InputLabel>
                      <Input id="country" name="country" autoComplete="country" autoFocus />
                    </FormControl>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      color="primary"
                      className={this.props.classes.submit}
                      onClick={this.ButtonClicked}
                    >
                      Iniciar Sesión
                    </Button>
                  </form>
                  <Typography component="p" className={this.props.classes.registration}>
                    Aún no tienes cuenta? <span className={this.props.classes.registrationText}>Registro</span>
                  </Typography>
                </Paper>
              </main>
            </MuiThemeProvider>
        );
    }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);