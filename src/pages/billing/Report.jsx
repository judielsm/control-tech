import React from 'react';
import Layout from '../../layouts/Main';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField'
import global from '../global';
import CustomTable from '../map/CustomTable';

import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

const styles = theme => ({
    report: {
        backgroundColor: global.mainButtonColor,
        padding: '6px 30px',
        width: 'auto',
        height: '50px',
        display: 'flex',
        margin: '0 auto',
    },
    formControl: {
        width: '200px'
    },
    download: {
        color: '#54A920',
        textDecoration: 'underline',
        position: 'absolute',
        right: '120px',
        top:'50%',
        fontSize: '18px'
    },
    xls: {
        width: '50px',
        fill: '#96CE2C',
        float: 'right',
        marginRight: '25px',
        marginTop: '25px'
    },
    downloadContainer:{
        position: 'relative'
    }
});
class Report extends React.Component  {
    state = {
        showTable: false
      };
    constructor(props){
        super(props);
        this.showTableClick = this.showTableClick.bind(this);
    }
    showTableClick(){
        
        this.setState({
            showTable: true
        });
    }
    render() {
        
        return (
            <Layout spacing={40}>
                <Grid item xs={12} sm={12}>
                    <Typography component="h4" variant="h4">
                        Facturación > Generar Reporte
                    </Typography>
                </Grid>


                <Grid item xs={4} sm={4}>
                    <TextField
                        className={this.props.classes.formControl}
                        id="date"
                        label="Fecha de inicio"
                        type="date"
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                </Grid>
                <Grid item xs={4} sm={4}>
                    <TextField
                        className={this.props.classes.formControl}
                        id="date"
                        label="Fecha de fin"
                        type="date"
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                </Grid>
                <Grid item xs={4} sm={4}>   
                    <Button
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={this.props.classes.report}
                        onClick={this.showTableClick}
                    >
                        GENERAR
                    </Button>
                </Grid>
                <Grid item xs={12} sm={12} className={this.props.classes.tables}>
                    {this.state.showTable && 
                    
                        <Paper className={this.props.classes.root}>
                            <CustomTable />
                        </Paper>
                    }
                </Grid>
                <Grid item xs={12} sm={12} className={this.props.classes.downloadContainer}>
                    {this.state.showTable && 
                        <div>
                            <span className={this.props.classes.download}>Descargar</span>
                            <img src="/images/xls.png" alt="xls" className={this.props.classes.xls}/>
                            
                        </div>
                    }
                </Grid>
            </Layout>
        );
    }
}

Report.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(Report);