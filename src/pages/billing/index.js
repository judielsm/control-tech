import Base from './Base';
import Report from './Report';
import Parameters from './Parameters';
export default {
    Base,
    Parameters,
    Report
} 