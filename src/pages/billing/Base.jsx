import React from 'react';
import Layout from '../../layouts/Main';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import { ReactComponent as Mixer } from '../../assets/Union.svg';
import { ReactComponent as Van } from '../../assets/icon.svg';





import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

const styles = theme => ({
    container: {
        height: '600px',
        position: 'relative'
        
    },

    circleMixers: {
        width: '300px',
        height: '300px',
        borderRadius: '50%',
        backgroundImage: 'linear-gradient(60deg, #EB6E31, #FCE055)',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
    },
    circleVehicles: {
        width: '300px',
        height: '300px',
        borderRadius: '50%',
        backgroundImage: 'linear-gradient(60deg, #246398, #6DD1F0)',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto'
    },
    iconMixerContainer:{
        width: '98%',
        height: '98%',
        backgroundColor: '#fff',
        borderRadius: '50%',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto'
    },
    iconVehiclesContainer:{
        width: '98%',
        height: '98%',
        backgroundColor: '#fff',
        borderRadius: '50%',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto'
    },
    MixerIcon:{
        width: '120px',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
    },
    vehicleIcon:{
        width: '180px',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
    },
    vanIcon:{
        width: '150px',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
    },
    MixerText:{
        width: '100%',
        fontSize: '24px',
        textAlign: 'center',
        color: '#EC8153',
        position: 'absolute',
        bottom: '10%',
        left: '0',
        right: '0',
        margin: 'auto'
    },
    vehicleText:{
        width: '100%',
        fontSize: '24px',
        textAlign: 'center',
        color: '#246398',
        position: 'absolute',
        bottom: '10%',
        left: '0',
        right: '0',
        margin: 'auto'
    }
});
class Base extends React.Component  {

    render() {
        const {classes} = this.props;
        return (
            <Layout spacing={40}>
                <Grid item xs={12} sm={12}>
                    <Typography component="h4" variant="h4">
                            Facturación
                    </Typography>
                </Grid>
                <Grid xs={2} sm={2}/>
                <Grid item xs={4} sm={4} >
                    <Link  to={'/billing/parameters'}>
                        <Paper  className={classes.container}>
                                <div className={classes.circleMixers}>
                                    <div className={classes.iconMixerContainer}>
                                        <Mixer className={classes.MixerIcon}/>
                                    </div>
                                </div>
                                <div className={classes.MixerText}>
                                    PARÁMETROS
                                </div>
                        </Paper>
                    
                    </Link>
                </Grid>
                <Grid item xs={4} sm={4}>
                    <Link  to={'/billing/report'}>
                        <Paper  className={classes.container}>
                                <div className={classes.circleVehicles}>
                                    <div className={classes.iconVehiclesContainer}>
                                        <img src="/images/icon.png" alt="" className={classes.vanIcon}/>
                                    </div>
                                </div>
                                <div className={classes.vehicleText}>
                                    GENERAR REPORTE
                                </div>
                        </Paper>
                    </Link>
                </Grid>
               
            </Layout>
        );
    }
}

Base.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(Base);