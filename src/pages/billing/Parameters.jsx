import React from 'react';
import Layout from '../../layouts/Main';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';

import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

const styles = theme => ({
    Parameters: {
        background: '#fff',
        color: '#215E94',
        padding: '6px 30px',
        width: 'auto',
        height: '50px',
        display: 'flex',
        margin: '0 auto',
        borderColor: '#215E94',
        borderStyle: 'solid',
        borderWidth: 'thin'

    },
    ParametersActive: {
        background: '#215E94',
        color: '#fff',
        padding: '6px 30px',
        width: 'auto',
        height: '50px',
        display: 'flex',
        margin: '0 auto',
        borderColor: '#215E94',
        borderStyle: 'solid',
        borderWidth: 'thin'

    },
    paper:{
        minHeight: '500px',
        position: 'relative'
    },
    save:{
        padding: '6px 30px',
        width: 'auto',
        height: '50px',
        display: 'flex',
        margin: '0 auto'
    },
    footer: {
        position: 'absolute',
        bottom: '0',
        left: '0',
        right: '0',
        margin: 'auto'
    },
    add:{
        color: '#000',
        fontWeight: 'bold',
        fontSize: '18px',
        marginLeft: '5px'

    },

});

const params = {
    id: null,
    unity: '',
    price: ''
}
class Parameters extends React.Component  {
    state = {
        vehicle:[params],
        user:[params],
        kilometer: [params],
        vehicleState: false,
        userState: false,
        kilometerState: false,
        vehicleHover: false,
        userHover: false,
        kilometerHover: false,

      };
    constructor(params){
        super(params);
        this.activeVehicle = this.activeVehicle.bind(this);
        this.activeUser = this.activeUser.bind(this);
        this.activeKilometer = this.activeKilometer.bind(this);
        this.addUnit = this.addUnit.bind(this);

    }

    addUnit(){
        let elements;
        if (this.state.vehicleState) {
            elements = document.querySelectorAll('[data-type="vehicleInputs"]');
            const element  = elements[elements.length -1];
            const inputs = element.querySelectorAll('input');
            const unity = inputs[0].value;
            const price = inputs[1].value;
            const index = element.getAttribute('data-id');
            
            const vehicle = this.state.vehicle;
            vehicle[index]={
                id: index,
                unity,
                price
            };
            vehicle.push(params)
            this.setState({vehicle});
           
        } else if(this.state.userState) {
            elements = document.querySelectorAll('[data-type="userInputs"]'); 
            const element  = elements[elements.length -1];
            const inputs = element.querySelectorAll('input');
            const unity = inputs[0].value;
            const price = inputs[1].value;
            const index = element.getAttribute('data-id');
                 
            const user = this.state.user;
            user[index]={
                id: index,
                unity,
                price
            };
            user.push(params)
            this.setState({user});
        }else {
            elements = document.querySelectorAll('[data-type="kilometerInputs"]');
            const element  = elements[elements.length -1];
            const inputs = element.querySelectorAll('input');
            const unity = inputs[0].value;
            const price = inputs[1].value;
            const index = element.getAttribute('data-id');  
            const kilometer = this.state.kilometer;
            kilometer[index]={
                id: index,
                unity,
                price
            };
            kilometer.push(params)
            this.setState({kilometer});
        }
        
    }
    resetStates(){
        this.setState({
            vehicleState: false,
            userState: false,
            kilometerState: false,
        });
    }
    activeVehicle(){
        this.resetStates();
        this.setState({
            vehicleState: true
        });
    }
    activeUser(){
        this.resetStates();
        this.setState({
            userState: true
        });
    }
    activeKilometer(){
        this.resetStates();
        this.setState({
            kilometerState: true
        });
    }

    render() {
        
        return (
            <Layout spacing={40}>
                <Grid item xs={12} sm={12}>
                    <Typography component="h4" variant="h4">
                        Facturación > Parámetros
                    </Typography>
                </Grid>


                <Grid item xs={3} sm={3}>
                    <Button
                        id="vehicleButton"
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={[
                            this.state.vehicleState?
                            this.props.classes.ParametersActive
                            :this.props.classes.Parameters
                        ]
                        }
                        onClick={this.activeVehicle}

                    >
                        Por Vehículo
                    </Button>
                </Grid>
                <Grid item xs={3} sm={3}>
                    <Button
                        id="userButton"
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={
                            [
                                this.state.userState?
                                this.props.classes.ParametersActive
                                :this.props.classes.Parameters
                                
                            ]
                        }
                        onClick={this.activeUser}
        
                    >
                        Por Usuario
                    </Button>
                </Grid>
                <Grid item xs={3} sm={3}>   
                    <Button
                        id="kilometerButton"
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={
                            [
                                this.state.kilometerState?
                                this.props.classes.ParametersActive
                                :this.props.classes.Parameters
                            ]
                        }
                        onClick={this.activeKilometer}
                
                    >
                        Por kilómetro
                    </Button>
                </Grid>
                <Grid item xs={12} sm={12}>
                {(this.state.vehicleState || this.state.userState || this.state.kilometerState) &&
                    <Paper className={this.props.classes.paper}>
                        <Grid container spacing={16}>
                            <Grid item xs={8} sm={8}> 
                                {this.state.vehicleState &&
                                    this.state.vehicle.map((param, index)=>{

                                        return(
                                            <div key={index} data-id={index} data-type="vehicleInputs">
                                                <TextField
                                                    className="unityInput"
                                                    name="unidad"
                                                    defaultValue={param.unity}
                                                    data-id={index}
                                                    label="Unidad"
                                                    margin="normal"
                                                />
                                                <TextField
                                                    className="unityInput"                                                    
                                                    name="precio"
                                                    defaultValue={param.price}
                                                    data-id={index}
                                                    label="Precio"
                                                    margin="normal"
                                                />
                                            </div>
                                        );
                                    })
                                }
                                {this.state.userState &&
                                    this.state.user.map((param, index)=>{

                                        return(
                                            <div key={index} data-id={index} data-type="userInputs">
                                                <TextField
                                                    className="unityInput"                                                    
                                                    name="unidad"
                                                    defaultValue={param.unity}
                                                    data-id={index}
                                                    label="Unidad"
                                                    margin="normal"
                                                />
                                                <TextField
                                                    className="unityInput"
                                                    name="precio"
                                                    defaultValue={param.price}
                                                    data-id={index}
                                                    label="Precio"
                                                    margin="normal"
                                                />
                                            </div>
                                        );
                                    })
                                }
                                {this.state.kilometerState &&
                                    this.state.kilometer.map((param, index)=>{

                                        return(
                                            <div key={index} data-id={index} data-type="kilometerInputs">
                                                <TextField
                                                    className="unityInput"
                                                    name="unidad"
                                                    defaultValue={param.unity}
                                                    data-id={index}
                                                    label="Unidad"
                                                    margin="normal"
                                                />
                                                <TextField
                                                    className="unityInput"
                                                    name="precio"
                                                    defaultValue={param.price}
                                                    data-id={index}
                                                    label="Precio"
                                                    margin="normal"
                                                />
                                            </div>
                                        );
                                    })
                                }
                            </Grid>
                            <Grid item xs={4} sm={4} className="add"> 
                                <Fab color="primary" aria-label="Add" onClick={this.addUnit}>
                                    <AddIcon />
                                </Fab>
                                <span className={this.props.classes.add}>Agregar unidad de facturación</span>
                            </Grid>
                            <Grid item xs={12} sm={12} className={this.props.classes.footer}> 
                                <Button
                                    type="button"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={this.props.classes.save}
                                    onClick={this.save}
                            
                                >
                                    Guardar
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                }
                </Grid>
            </Layout>
        );
    }
}

Parameters.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(Parameters);