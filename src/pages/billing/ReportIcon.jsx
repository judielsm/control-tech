import React from 'react';

import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import { ReactComponent as Doc } from '../../assets/document.svg';
import { ReactComponent as Van } from '../../assets/volkswagen.svg';


const styles = theme => ({ 
    container: {
        position: 'relative',
    },
    doc:{
        width: '180px',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
        fill: '#3C86B4'
    },
    van:{
        position: 'absolute',
        top: '40%',
        left: '100%'
    }
});
class ReportIcon extends React.Component {
    render(){
        return(
            <div className={this.props.classes.container}>
                <Doc  className={this.props.classes.doc}/>
                <Van className={this.props.classes.van}/>
            </div>
        );
    }
}

ReportIcon.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(ReportIcon);