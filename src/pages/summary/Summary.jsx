import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Chart from 'react-google-charts';
import Button from '@material-ui/core/Button';
import Layout from '../../layouts/Main';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import global from '../global';

const styles = theme => ({
    report: {
        backgroundColor: global.mainButtonColor,
        padding: '6px 30px',    
        height: '50px',
        display: 'flex',
        margin: '0 auto',
    },
    date: {
        width: '250px'
    }
});
class Summary extends Component {

    render() {
    const { classes } = this.props;

        return (
            <Layout spacing={40}>
                <Grid item xs={12} sm={12}>
                    <Typography component="h4" variant="h4">
                            Resumen
                    </Typography>

                </Grid>

                <Grid item xs={6} sm={6}>
                    <TextField
                        className={classes.date}
                        id="date"
                        label="Fecha de inicio"
                        type="date" 
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                </Grid>
                <Grid item xs={3} sm={3}>
                    <TextField
                        className={classes.date}
                        id="date"
                        label="Fecha de fin"
                        type="date"
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                </Grid>
                <Grid item xs={3} sm={3}>
                    <Button
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={this.props.classes.report}
                    >
                        VER REPORTE
                    </Button>
                </Grid>


                <Grid item xs={12} sm={6}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                PRECIO POR PERSONA
                            </Typography>
                            <Typography component="p" className="summaryValue">
                                $20 USD
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                UTILIZACIÓN
                            </Typography>
                            <Typography component="p" className="summaryValue">
                                68%
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                GRÁFICO ÚLTIMOS 12 MESES
                            </Typography>
                            <Chart
                                width={'100%'}
                                chartType="LineChart"
                                loader={<div>Loading Chart</div>}
                                data={[
                                    ['x', 'dogs'],
                                    [0, 0],
                                    [1, 10],
                                    [2, 23],
                                    [3, 17],
                                    [4, 18],
                                    [5, 9],
                                    [6, 11],
                                    [7, 27],
                                    [8, 33],
                                    [9, 40],
                                    [10, 32],
                                    [11, 35],
                                ]}
                                options={{
                                    hAxis: {
                                    title: '',
                                    },
                                    vAxis: {
                                    title: '',
                                    },
                                }}
                                rootProps={{ 'data-testid': '1' }}
                            />
                        </CardContent>

                    </Card>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom>
                                GRÁFICO ÚLTIMOS 12 MESES
                            </Typography>
                            <Chart
                                width={'100%'}
                                chartType="LineChart"
                                loader={<div>Loading Chart</div>}
                                data={[
                                    ['x', 'dogs'],
                                    [0, 0],
                                    [1, 10],
                                    [2, 23],
                                    [3, 17],
                                    [4, 18],
                                    [5, 9],
                                    [6, 11],
                                    [7, 27],
                                    [8, 33],
                                    [9, 40],
                                    [10, 32],
                                    [11, 35],
                                ]}
                                options={{
                                    hAxis: {
                                    title: '',
                                    },
                                    vAxis: {
                                    title: '',
                                    },
                                }}
                                rootProps={{ 'data-testid': '1' }}
                            />
                        </CardContent>

                    </Card>
                </Grid>

                <Grid item xs={3} sm={3}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                PASAJEROS TRANSPORTADOS
                            </Typography>
                            <Typography component="p" className="summaryValue">
                                28
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={3} sm={3}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                PASAJEROS PROGRAMADOS
                            </Typography>
                            <Typography component="p" className="summaryValue">
                                42
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={3} sm={3}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                VEHICULOS PROGRAMADOS
                            </Typography>
                            <Typography component="p" className="summaryValue">
                                3
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={3} sm={3}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                CAPACIDAD TOTAL DE VEHICULOS
                            </Typography>
                            <Typography component="p" className="summaryValue">
                                42
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={3} sm={3}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                SITIO 1
                            </Typography>
                            <Typography component="p" className="summaryValue">
                                $ 20 USD
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={3} sm={3}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                SITIO 2
                            </Typography>
                            <Typography component="p" className="summaryValue">
                                $ 23 USD
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={3} sm={3}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                SITIO 1
                            </Typography>
                            <Typography component="p" className="summaryValue">
                                $ 20 USD
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={3} sm={3}>
                    <Card>
                        <CardContent>
                            <Typography  color="textSecondary" gutterBottom className="summaryTitle">
                                SITIO 2
                            </Typography>
                            <Typography component="p" className="summaryValue">
                                $ 23 USD
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
            </Layout>
        );
    }
}

Summary.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(Summary);