import React from 'react';
import Layout from '../../layouts/Main';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FilledInput from '@material-ui/core/FilledInput';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import MapCmp from './MapCmp';
import global from '../global';


import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

const styles = theme => ({
    report: {
        backgroundColor: global.mainButtonColor,
        padding: '6px 30px',
        width: 'auto',
        height: '50px',
        display: 'flex',
        margin: 'auto',
        fontSize: '18px',
        bottom: '0',
        top: '0',
        position: 'absolute',
    
    },
    formControl: {
        width: '100%'
    }
});
class Map extends React.Component  {
    state = {
        vehicle: [],
        plate: "",
    }
    handleChangeVehicle =  event => {
        
        this.setState({ vehicle: event.target.value });
    }
    handleChangePlate =  event => {
        this.setState({ plate: event.target.value });
    };
    render() {
        
        return (
            <Layout spacing={40}>
                <Grid item xs={12} sm={12}>
                    <Typography component="h4" variant="h4">
                            Mapa
                    </Typography>
                </Grid>

                <Grid item xs={4} sm={4}>
                    <Typography color="textSecondary" gutterBottom>
                            Estado del Vehiculo
                    </Typography>
                    <FormControl variant="filled" className={this.props.classes.formControl}>
                        <Select
                            multiple
                            value={this.state.vehicle}
                            input={<FilledInput name="age" id="filled-age-simple" />}
                            onChange={this.handleChangeVehicle}
                            className="dateSelect"
                            renderValue={selected => selected.join(',')}
                        >
                            <MenuItem value="">
                            <em>None</em>
                            </MenuItem>
                            <MenuItem value={'EN TRANSITO'}>
                                <div className="selectContainer">
                                    <div className="selectCircle circleYellow"></div>
                                    <span>EN TRANSITO</span>
                                    <Checkbox 
                                        checked={this.state.vehicle.find(vehicle => {return vehicle === "EN TRANSITO"})? true : false}
                                        className="selectCheck" 
                                        value="transit" 
                                    />
                                </div>
                            </MenuItem>
                            <MenuItem value={'PUESTO EN SITIO'}>
                                <div className="selectContainer">
                                    <div className="selectCircle circleGreen"></div>
                                    <span>PUESTO EN SITIO</span>
                                    <Checkbox 
                                        checked={this.state.vehicle.find(vehicle => {return vehicle === "PUESTO EN SITIO"})? true : false}
                                        
                                    className="selectCheck" value="checked" />
                                </div>
                            </MenuItem>
                            <MenuItem value={'EN SERVICIO'}>
                                <div className="selectContainer">
                                    <div className="selectCircle circleBlue"></div>
                                    <span>EN SERVICIO</span>
                                    <Checkbox 
                                        checked={this.state.vehicle.find(vehicle => {return vehicle === "EN SERVICIO"})? true : false}
                                    
                                    className="selectCheck" value="checked" />
                                </div>
                            </MenuItem>
                            <MenuItem value={'10 MINUTOS'}>
                                <div className="selectContainer">
                                    <div className="selectCircle circleDarkBlue"></div>
                                    <span>10 MINUTOS</span>
                                    <Checkbox 
                                        checked={this.state.vehicle.find(vehicle => {return vehicle === "10 MINUTOS"})? true : false}
                                    
                                    className="selectCheck" value="checked" />
                                </div>
                            </MenuItem>
                            <MenuItem value={'12 HORAS'}>
                                <div className="selectContainer">
                                    <div className="selectCircle circlePurple"></div>
                                    <span>12 HORAS</span>
                                    <Checkbox 
                                        checked={this.state.vehicle.find(vehicle => {return vehicle === "12 HORAS"})? true : false}
                                    
                                    className="selectCheck" value="checked" />
                                </div>
                            </MenuItem>
                            <MenuItem value={'24 HORAS'}>
                                <div className="selectContainer">
                                    <div className="selectCircle circleRed"></div>
                                    <span>24 HORAS</span>
                                    <Checkbox
                                        checked={this.state.vehicle.find(vehicle => {return vehicle === "24 HORAS"})? true : false}
                                    
                                    className="selectCheck" value="checked" />
                                </div>
                            </MenuItem>
                            <MenuItem value={'15 DIAS'}>
                                <div className="selectContainer">
                                    <div className="selectCircle circleYellow"></div>
                                    <span>15 DIAS</span>
                                    <Checkbox
                                        checked={this.state.vehicle.find(vehicle => {return vehicle === "15 DIAS"})? true : false}
                                    
                                    className="selectCheck" value="checked" />
                                </div>
                            </MenuItem> 
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={4} sm={4}>
                    <Typography color="textSecondary" gutterBottom>
                            Placa del Vehiculo
                    </Typography>
                    <FormControl variant="filled" className={this.props.classes.formControl}>
                        <Select
                            value={this.state.plate}
                            input={<FilledInput name="age" id="filled-age-simple" />}
                            onChange={this.handleChangePlate}
                            className="dateSelect"
                            
                        >
                            <MenuItem value="">
                            <em>None</em>
                            </MenuItem>
                            <MenuItem value={10}>1ABC234</MenuItem>
                            <MenuItem value={20}>1ASC234</MenuItem>
                            <MenuItem value={30}>2FBC234</MenuItem>
                            <MenuItem value={40}>1ABC209</MenuItem>

                            <MenuItem value={50}>17BC234</MenuItem>
                            <MenuItem value={60}>1ASGH34</MenuItem>
                            <MenuItem value={70}>1ABL548</MenuItem>
                            <MenuItem value={80}>1ASA896</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={4} sm={4} style={{position: 'relative'}}>
                    <Button
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={this.props.classes.report}
                        onClick={()=>{this.props.history.push('/map/listing')}}
                    >
                        VER LISTADO
                    </Button>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <MapCmp 
                        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAX2p1HrCB96w0QqI7t4fhs3w2H2WOFgN4"
                        loadingElement={<div style={{ height: `100%` }} />}
                        containerElement={<div style={{ height: `100vh` }} />}
                        mapElement={<div style={{ height: `100%` }} />}
                    />
                </Grid>

            </Layout>
        );
    }
}

Map.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(Map);