import React from 'react';
import Layout from '../../layouts/Main';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import CustomTable from './CustomTable';

import Typography from '@material-ui/core/Typography';


import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    table: {
      minWidth: 700,
    },
  });


class List extends React.Component  {

    render() {
        const { classes } = this.props;
        return (
            <Layout spacing={40}>
                <Grid item xs={12} sm={12}>
                    <Typography component="h4" variant="h4">
                    {
                        //TODO: create breadcrumb componet
                    }
                            Mapa > Listado
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Paper className={classes.root}>
                        <CustomTable />
                    </Paper>
                </Grid>
            </Layout>
        );
    }
}

List.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(List);