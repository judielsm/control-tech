import React from 'react';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

const styles = theme => ({
    table: {
      minWidth: 700,
    },
  });
class CustomTable extends React.Component {
    render(){
        const { classes } = this.props;

        return(
            <Table className={classes.table}>
                            <TableHead>
                            <TableRow>
                                <TableCell>Mercancía</TableCell>
                                <TableCell align="center">Precio</TableCell>
                                <TableCell align="center">Visita</TableCell>
                                <TableCell align="center">Inventario</TableCell>
                                <TableCell align="center">Volumen de venta</TableCell>
                                <TableCell align="center">Tiempo</TableCell>
                                <TableCell align="center">Operación</TableCell>
                                <TableCell align="center">Estado</TableCell>

                            </TableRow>
                            </TableHead>
                            <TableBody>
                            
                                <TableRow >
                                    <TableCell align="center">Sold wood desk </TableCell>
                                    <TableCell align="center">$500</TableCell>
                                    <TableCell align="center">468765</TableCell>
                                    <TableCell align="center">200</TableCell>
                                    <TableCell align="center">100</TableCell>
                                    <TableCell align="center">2019. 02. 01 20: 21 </TableCell>
                                    <TableCell align="center" className="tableLink">Mas detalles</TableCell>
                                    <TableCell align="center" className="tableStatusOK">OK</TableCell>

                                </TableRow>
                                <TableRow >
                                    <TableCell align="center">Closet </TableCell>
                                    <TableCell align="center">$500</TableCell>
                                    <TableCell align="center">468765</TableCell>
                                    <TableCell align="center">200</TableCell>
                                    <TableCell align="center">100</TableCell>
                                    <TableCell align="center">2019. 02. 01 20: 21 </TableCell>
                                    <TableCell align="center" className="tableLink">Mas detalles</TableCell>
                                    <TableCell align="center" className="tableStatusFin">Finalizado</TableCell>

                                </TableRow>
                                <TableRow >
                                    <TableCell align="center">Dinig table </TableCell>
                                    <TableCell align="center">$500</TableCell>
                                    <TableCell align="center">468765</TableCell>
                                    <TableCell align="center">200</TableCell>
                                    <TableCell align="center">100</TableCell>
                                    <TableCell align="center">2019. 02. 01 20: 21 </TableCell>
                                    <TableCell align="center" className="tableLink">Mas detalles</TableCell>
                                    <TableCell align="center" className="tableStatusPen">Pendiente</TableCell>

                                </TableRow>
                                <TableRow >
                                    <TableCell align="center">Night table </TableCell>
                                    <TableCell align="center">$500</TableCell>
                                    <TableCell align="center">468765</TableCell>
                                    <TableCell align="center">200</TableCell>
                                    <TableCell align="center">100</TableCell>
                                    <TableCell align="center">2019. 02. 01 20: 21 </TableCell>
                                    <TableCell align="center" className="tableLink">Mas detalles</TableCell>
                                    <TableCell align="center">Doing</TableCell>

                                </TableRow>
                                <TableRow >
                                    <TableCell align="center">Table lamp </TableCell>
                                    <TableCell align="center">$500</TableCell>
                                    <TableCell align="center">468765</TableCell>
                                    <TableCell align="center">200</TableCell>
                                    <TableCell align="center">100</TableCell>
                                    <TableCell align="center">2019. 02. 01 20: 21 </TableCell>
                                    <TableCell align="center" className="tableLink">Mas detalles</TableCell>
                                    <TableCell align="center">Doing</TableCell>

                                </TableRow>
                                <TableRow >
                                    <TableCell align="center">Sold wood desk </TableCell>
                                    <TableCell align="center">$500</TableCell>
                                    <TableCell align="center">468765</TableCell>
                                    <TableCell align="center">200</TableCell>
                                    <TableCell align="center">100</TableCell>
                                    <TableCell align="center">2019. 02. 01 20: 21 </TableCell>
                                    <TableCell align="center" className="tableLink">Mas detalles</TableCell>
                                    <TableCell align="center">Doing</TableCell>

                                </TableRow>
                                <TableRow >
                                    <TableCell align="center">Table lamp  </TableCell>
                                    <TableCell align="center">$500</TableCell>
                                    <TableCell align="center">468765</TableCell>
                                    <TableCell align="center">200</TableCell>
                                    <TableCell align="center">100</TableCell>
                                    <TableCell align="center">2019. 02. 01 20: 21 </TableCell>
                                    <TableCell align="center" className="tableLink">Mas detalles</TableCell>
                                    <TableCell align="center">Doing</TableCell>

                                </TableRow>
                                <TableRow >
                                    <TableCell align="center">Sold wood desk </TableCell>
                                    <TableCell align="center">$500</TableCell>
                                    <TableCell align="center">468765</TableCell>
                                    <TableCell align="center">200</TableCell>
                                    <TableCell align="center">100</TableCell>
                                    <TableCell align="center">2019. 02. 01 20: 21 </TableCell>
                                    <TableCell align="center" className="tableLink">Mas detalles</TableCell>
                                    <TableCell align="center">Doing</TableCell>

                                </TableRow>
                                
                           
                            </TableBody>
                        </Table>
        );
    }
}
CustomTable.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(CustomTable);