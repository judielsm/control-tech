import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, OverlayView } from "react-google-maps"


const plates =[
    {
        name: '1ABC1234',
        class: 'transit',
        location: {
            lat: 4.6567603,
            lng: -74.1177127,
        }
    },
    {
        name: '2ABC4567',
        class: 'transit',
        location: {
            lat: 4.6567603,
            lng: -74.1977127,
        }
    },
    {
        name: '2DEF4567',
        class: 'transit',
        location: {
            lat: 4.6567603,
            lng: -74.2560127,
        }
    },
    {
        name: '2XCV4567',
        class: 'site',
        location: {
            lat: 4.9987603,
            lng: -74.2560127,
        }
    },
    {
        name: '8TYU4567',
        class: 'site',
        location: {
            lat: 4.6976603,
            lng: -74.2560127,
        }
    },
    {
        name: '0LAP4567',
        class: 'site',
        location: {
            lat: 4.9052603,
            lng: -74.9860127,
        }
    },
    {
        name: '2QSD4567',
        class: 'service',
        location: {
            lat: 4.3567603,
            lng: -74.9560127,
        }
    },
    {
        name: '6FLK4567',
        class: 'service',
        location: {
            lat: 4.7937603,
            lng: -74.1260127,
        }
    },
    {
        name: '2PNB4567',
        class: 'service',
        location: {
            lat: 4.8967603,
            lng: -74.0660127,
        }
    }
];

class MapCmp extends React.Component {
    state ={
        plates
    }
    render(){
        return(
            <GoogleMap
            defaultZoom={12}
            defaultCenter={{ lat: 4.648625, lng: -74.2478914 }}
            >
            { this.state.plates.map((plate)=>{
                return (<OverlayView
                            position={plate.location}
                            mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
                        >
                            <div className={`mapMarker ${plate.class}`}>
                                <span style={{ color: `#fff` }}> {plate.name}</span>
                            </div>
                        </OverlayView>);

            })

            }
            </GoogleMap>

        );
    }
}


export default withScriptjs(withGoogleMap(MapCmp));
