import LoadVehicles from './LoadVehicles';
import SeeVehicles from './SeeVehicles';
import Programation from './Programation';
import LoadUsers from './LoadUsers';
import SeeUsers from './SeeUsers';

export default {
    SeeVehicles,
    SeeUsers,
    LoadUsers,
    Programation,
    LoadVehicles
};