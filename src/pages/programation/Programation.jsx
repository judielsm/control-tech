import React from 'react';
import Layout from '../../layouts/Main';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

const styles = theme => ({
    container: {
        height: '600px',
        position: 'relative'
        
    },

    circleUsers: {
        width: '300px',
        height: '300px',
        borderRadius: '50%',
        backgroundImage: 'linear-gradient(60deg, #EB6E31, #FCE055)',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
    },
    circleVehicles: {
        width: '300px',
        height: '300px',
        borderRadius: '50%',
        backgroundImage: 'linear-gradient(60deg, #246398, #6DD1F0)',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto'
    },
    iconUserContainer:{
        width: '98%',
        height: '98%',
        backgroundColor: '#fff',
        borderRadius: '50%',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto'
    },
    iconVehiclesContainer:{
        width: '98%',
        height: '98%',
        backgroundColor: '#fff',
        borderRadius: '50%',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto'
    },
    userIcon:{
        width: '120px',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
        fill: '#F19A3F',
    },
    vehicleIcon:{
        width: '180px',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
        fill: '#3C86B4'
    },
    userText:{
        width: '100%',
        fontSize: '24px',
        textAlign: 'center',
        color: '#EC8153',
        position: 'absolute',
        bottom: '10%',
        left: '0',
        right: '0',
        margin: 'auto'
    },
    vehicleText:{
        width: '100%',
        fontSize: '24px',
        textAlign: 'center',
        color: '#246398',
        position: 'absolute',
        bottom: '10%',
        left: '0',
        right: '0',
        margin: 'auto'
    }
});
class Programation extends React.Component  {

    render() {
        const {classes} = this.props;
        return (
            <Layout spacing={40}>
                <Grid item xs={12} sm={12}>
                    <Typography component="h4" variant="h4">
                            Programación
                    </Typography>
                </Grid>
                <Grid xs={2} sm={2}/>
                <Grid item xs={4} sm={4} >
                    <Link  to={'/programation/users/load'}>
                        <Paper  className={classes.container}>
                                <div className={classes.circleUsers}>
                                    <div className={classes.iconUserContainer}>
                                        <img src="/images/User.png" alt="" srcSet="" className={classes.userIcon}/>
                                    </div>
                                </div>
                                <div className={classes.userText}>
                                    USUARIOS
                                </div>
                        </Paper>
                    
                    </Link>
                </Grid>
                <Grid item xs={4} sm={4}>
                    <Link  to={'/programation/vehicles/load'}>
                        <Paper  className={classes.container}>
                                <div className={classes.circleVehicles}>
                                    <div className={classes.iconVehiclesContainer}>
                                    <img src="/images/vehicle.png" alt="" srcSet="" className={classes.vehicleIcon}/>
                                    </div>
                                </div>
                                <div className={classes.vehicleText}>
                                    VEHICULOS
                                </div>
                        </Paper>
                    </Link>
                </Grid>
               
            </Layout>
        );
    }
}

Programation.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(Programation);