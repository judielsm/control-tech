import React from 'react';
import Layout from '../../layouts/Main';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField'
import global from '../global';

import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

const styles = theme => ({
    report: {
        backgroundColor: global.mainButtonColor,
        padding: '6px 30px',
        width: 'auto',
        height: '50px',
        display: 'flex',
        margin: '0 auto',
    },
    formControl: {
        width: '200px'
    },
    firstMessage: {
        width: '100%',
        height: '100px',
        position: 'relative',
        background: 'linear-gradient(to right, #1D5B92,#6ED3F2)',
        borderRadius: '20px',
        color: '#122dff',
        display: 'inline-block',
        padding: '1px',
        textDecoration: 'none',
    },
    firstText: {
        color: '#1D5B92',
        fontSize: '20px',
        fontWeight: 'normal',
        marginLeft: '25px',
        position: 'absolute',
        top: '20%',
        background: '#fff',
        display: 'block',
        padding: '1em 3em',
        borderRadius: '6px'
    },
    xls: {
        width: '50px',
        fill: '#96CE2C',
        float: 'right',
        marginRight: '25px',
        marginTop: '25px'
    },
    first: {
        marginBottom: '50px'
    },
    second: {
        marginTop: '50px'
    },
    secondText: {
        color: '#1D5B92',
        fontSize: '20px',
        fontWeight: 'normal',
        position: 'absolute',
        top: '20%',
        background: '#fff',
        display: 'block',
        padding: '1em 3em',
        borderRadius: '6px',
        width: '100%',
        textAlign: 'center'
    },
    container: {
        width: '100%',
        height: '100%',
        background: '#fff',
        borderRadius: '20px',
        position: 'relative'
    }
});
class LoadUsers extends React.Component  {
    state = {
        country: '',
      };
    render() {
        
        return (
            <Layout spacing={40}>
                <Grid item xs={12} sm={12}>
                    <Typography component="h4" variant="h4">
                            Programación > Usuarios
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12} className={this.props.classes.first}>
                    <div className={this.props.classes.firstMessage}>
                        <div className={this.props.classes.container}>
                            <span className={this.props.classes.firstText}>Este archivo debe ser subido en el suguiente formato </span>
                                <img src="/images/xls.png" alt="xls" className={this.props.classes.xls}/>

                        </div>
                    </div>
                </Grid>
                <Grid item xs={3} sm={3}>
                    <Typography color="textSecondary" gutterBottom>
                         Pais
                    </Typography>
                    <FormControl className={this.props.classes.formControl}>
                        <Select
                            value={this.state.country}
                            onChange={this.handleChange}
                            inputProps={{
                            name: 'age',
                            id: 'age-simple',
                            }}
                        >
                            <MenuItem value="">
                            <em>None</em>
                            </MenuItem>
                            <MenuItem value={10}>Ten</MenuItem>
                            <MenuItem value={20}>Twenty</MenuItem>
                            <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={3} sm={3}>
                    <TextField
                        className={this.props.classes.date}
                        id="date"
                        label="Fecha de inicio"
                        type="date"
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                </Grid>
                <Grid item xs={3} sm={3}>
                    <TextField
                        className={this.props.classes.date}
                        id="date"
                        label="Fecha de inicio"
                        type="date"
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                </Grid>
                <Grid item xs={3} sm={3}>   
                    <Button
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={this.props.classes.report}
                    >
                        CARGAR
                    </Button>
                </Grid>
                <Grid item xs={12} sm={12} className={this.props.classes.second}>
                    <div className={this.props.classes.firstMessage}>
                        <div className={this.props.classes.container}>
                            <span className={this.props.classes.secondText}>Si necesitas modificar o agregar un nuevo usuario, carga nuevamente el archivo. </span>

                        </div>
                    </div>
                </Grid>

            </Layout>
        );
    }
}

LoadUsers.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(LoadUsers);