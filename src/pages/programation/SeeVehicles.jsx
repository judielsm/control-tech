import React from 'react';
import Layout from '../../layouts/Main';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CustomTable from '../map/CustomTable';
import Snack from './Snack';


import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

const styles = theme => ({

});
let id = 0;

class SeeVehicles extends React.Component  {

    render() {
        const { classes } = this.props;        
        return (
            <Layout spacing={40}>
                <Grid item xs={12} sm={12}>
                    <Typography component="h4" variant="h4">
                            Programación > Vehiculos
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Snack type={'error'}/>
                </Grid>
                <Grid item xs={12} sm={12}>
                    <Paper className={classes.root}>
                        <CustomTable />
                        
                    </Paper>    
                </Grid>
                

            </Layout>
        );
    }
}

SeeVehicles.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(SeeVehicles);