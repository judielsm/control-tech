import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import { ReactComponent as Check } from '../../assets/success.svg';
import { ReactComponent as Cancel } from '../../assets/cancel.svg';

import ClearIcon from '@material-ui/icons/Clear';

import PropTypes from 'prop-types';

const styles = theme => ({
    wrapperError: {
        width: '100%',
        height: '100px',
        position: 'relative',
        background: 'linear-gradient(to right, #ED733F,#FBD792)',
        borderRadius: '20px',
        color: '#122dff',
        display: 'inline-block',
        padding: '1px',
        textDecoration: 'none',
    },
    wrapperSuccess:{
        width: '100%',
        height: '100px',
        position: 'relative',
        background: 'linear-gradient(to right, #309220,#BAF79D)',
        borderRadius: '20px',
        color: '#122dff',
        display: 'inline-block',
        padding: '1px',
        textDecoration: 'none',
    },
    container: {
        width: '100%',
        height: '100%',
        background: '#fff',
        borderRadius: '20px'
    },
    text:{
        color: '#54A921',
        fontSize: '20px',
        fontWeight: 'normal',
        marginLeft: '25px',
        position: 'absolute',
        top: '38%',
        left: '10%',
        background: '#fff',
        display: 'block',
        borderRadius: '6px'
    },
    errorText:{
        color: '#ED733F',
        fontSize: '20px',
        fontWeight: 'normal',
        marginLeft: '25px',
        position: 'absolute',
        top: '38%',
        left: '10%',
        background: '#fff',
        display: 'block',
        borderRadius: '6px'
    },
    check: {
        width: '50px',
        fill: '#96CE2C',
        float: 'left',
        marginLeft: '25px',
        marginTop: '25px'
    },
    cancel: {
        width: '50px',
        fill: '#ED733F',
        float: 'left',
        marginLeft: '25px',
        marginTop: '25px'
    },
    clearSuccess: {
        position: 'absolute',
        right   : '10px',
        top: '10px',
        color: '#96CE2C'
    },
    clearError: {
        right   : '10px',
        top: '10px',
        position: 'absolute',
        color: '#ED733F'
    }
});
class Snack extends React.Component {
    render(){
        return (
            <div className={this.props.type === 'success'?this.props.classes.wrapperSuccess:this.props.classes.wrapperError}>
                <div className={this.props.classes.container}>
                    {this.props.type === 'success'?
                        <Check className={this.props.classes.check} />
                    :
                        <Cancel className={this.props.classes.cancel} />
                    }

                    <span className={this.props.type === 'success'? this.props.classes.text: this.props.classes.errorText }>
                        {this.props.type === 'success'?
                            'Archivo cargado exitosamente'
                            : 'El archivo no pudo ser cargado exitosamente'
                        }
                    </span>
                    <ClearIcon className={this.props.type === 'success'? this.props.classes.clearSuccess : this.props.classes.clearError }/>
                </div>
            </div>
        );
    }
}

Snack.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(Snack);