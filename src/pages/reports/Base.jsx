import React from 'react';
import Layout from '../../layouts/Main';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import { ReactComponent as Passenger } from '../../assets/passenger.svg';
import { ReactComponent as Route } from '../../assets/route.svg';
import { ReactComponent as Summary } from '../../assets/summary.svg';
import { ReactComponent as Gps } from '../../assets/gps.svg';
import { ReactComponent as User } from '../../assets/userCon.svg';
import { ReactComponent as Driver } from '../../assets/driver.svg';
import { ReactComponent as Velocity } from '../../assets/speed.svg';









import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';

const styles = theme => ({
    container: {
        height: '350px',
        position: 'relative'
        
    },

    circleFirst: {
        width: '180px',
        height: '180px',
        borderRadius: '50%',
        backgroundImage: 'linear-gradient(60deg, #EB6E31, #FCE055)',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '50px',
        margin: 'auto',
    },
    circleSecond: {
        width: '180px',
        height: '180px',
        borderRadius: '50%',
        backgroundImage: 'linear-gradient(60deg, #367D13, #9AC620)',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '50px',
        margin: 'auto',
    },
    circleThird: {
        width: '180px',
        height: '180px',
        borderRadius: '50%',
        backgroundImage: 'linear-gradient(60deg, #246398, #6DD1F0)',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '50px',
        margin: 'auto'
    },
    iconUserContainer:{
        width: '98%',
        height: '98%',
        backgroundColor: '#fff',
        borderRadius: '50%',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto'
    },
    iconVehiclesContainer:{
        width: '98%',
        height: '98%',
        backgroundColor: '#fff',
        borderRadius: '50%',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto'
    },
    firstIcon:{
        width: '120px',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
        fill: '#F19A3F',
    },
    secondIcon:{
        width: '120px',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
        fill: '#367D13',
    },
    thirdIcon:{
        width: '120px',
        position: 'absolute',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        margin: 'auto',
        fill: '#3C86B4'
    },
    firstText:{
        width: '100%',
        fontSize: '20px',
        textAlign: 'center',
        color: '#EC8153',
        position: 'absolute',
        bottom: '10%',
        left: '0',
        right: '0',
        margin: 'auto'
    },
    secondText:{
        width: '100%',
        fontSize: '20px',
        textAlign: 'center',
        color: '#367D13',
        position: 'absolute',
        bottom: '10%',
        left: '0',
        right: '0',
        margin: 'auto'
    },
    thirdText:{
        width: '100%',
        fontSize: '20px',
        textAlign: 'center',
        color: '#246398',
        position: 'absolute',
        bottom: '10%',
        left: '0',
        right: '0',
        margin: 'auto'
    },
    gridContainer: {
        marginLeft: '50px',
        marginRight: '50px'

    }
});
class Programation extends React.Component  {

    render() {
        const {classes} = this.props;
        return (
            <Layout spacing={40}>
                <Grid item xs={12} sm={12}>
                    <Typography component="h4" variant="h4">
                            Generar Reporte
                    </Typography>
                </Grid>
                <Grid container spacing={40} className={classes.gridContainer}>
                
                    <Grid item xs={4} sm={4} >
                        <Link  to={'/reports/transported'}>
                            <Paper  className={classes.container}>
                                    <div className={classes.circleFirst}>
                                        <div className={classes.iconUserContainer}>
                                            <Passenger className={classes.firstIcon}/>
                                        </div>
                                    </div>
                                    <div className={classes.firstText}>
                                        PASAJEROS TRANSPORTADOS
                                    </div>
                            </Paper>
                        
                        </Link>
                    </Grid>
                    <Grid item xs={4} sm={4} >
                        <Link  to={'/reports/historical'}>
                            <Paper  className={classes.container}>
                                    <div className={classes.circleSecond}>
                                        <div className={classes.iconUserContainer}>
                                            <Route className={classes.secondIcon} />
                                        </div>
                                    </div>
                                    <div className={classes.secondText}>
                                        HISTÓRICO DE RUTAS
                                    </div>
                            </Paper>
                        
                        </Link>
                    </Grid>
                    <Grid item xs={4} sm={4} >
                        <Link  to={'/reports/summary'}>
                            <Paper  className={classes.container}>
                                    <div className={classes.circleThird}>
                                        <div className={classes.iconUserContainer}>
                                            <Summary className={classes.thirdIcon} />
                                        </div>
                                    </div>
                                    <div className={classes.thirdText}>
                                        RESUMEN DE OPERACIÓN
                                    </div>
                            </Paper>
                        
                        </Link>
                    </Grid>
                    <Grid item xs={4} sm={4} >
                        <Link  to={'/reports/gps'}>
                            <Paper  className={classes.container}>
                                    <div className={classes.circleFirst}>
                                        <div className={classes.iconUserContainer}>
                                            <Gps className={classes.firstIcon} />
                                        </div>
                                    </div>
                                    <div className={classes.firstText}>
                                        REPORTE DE GPS
                                    </div>
                            </Paper>
                        
                        </Link>
                    </Grid>
                    <Grid item xs={4} sm={4} >
                        <Link  to={'/reports/app'}>
                            <Paper  className={classes.container}>
                                    <div className={classes.circleSecond}>
                                        <div className={classes.iconUserContainer}>
                                            <Driver className={classes.secondIcon}/>
                                        </div>
                                    </div>
                                    <div className={classes.secondText}>
                                        REPORTE DE UTILIZACIÓN DE APP DE CONDUCTORES
                                    </div>
                            </Paper>
                        
                        </Link>
                    </Grid>
                    <Grid item xs={4} sm={4} >
                        <Link  to={'/reports/users'}>
                            <Paper  className={classes.container}>
                                    <div className={classes.circleThird}>
                                        <div className={classes.iconUserContainer}>
                                            <User className={classes.thirdIcon} />
                                        </div>
                                    </div>
                                    <div className={classes.thirdText}>
                                        CONFIRMACIONES DE USUARIOS
                                    </div>
                            </Paper>
                        
                        </Link>
                    </Grid>
                    <Grid item xs={4} sm={4} >
                        <Link  to={'/reports/speed'}>
                            <Paper  className={classes.container}>
                                    <div className={classes.circleFirst}>
                                        <div className={classes.iconUserContainer}>
                                            <Velocity  className={classes.firstIcon} />
                                        </div>
                                    </div>
                                    <div className={classes.firstText}>
                                        REPORTE DE VELOCIDAD
                                    </div>
                            </Paper>
                        
                        </Link>
                    </Grid>
                </Grid>
                
            </Layout>
        );
    }
}

Programation.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default withStyles(styles)(Programation);