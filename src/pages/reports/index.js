import Base from './Base';
import Passengers from './TransportedPassengers';
import App from './App';
import Confirmations from './Confirmations';
import Gps from './Gps';
import Historical from './Historical';
import Velocity from './Velocity';
export default {
    Passengers,
    Confirmations,
    Historical,
    Velocity,
    Gps,
    App,
    Base
};