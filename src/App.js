import React, { Component } from 'react';
import { Route, } from 'react-router-dom';
import routes from './routes';

import './App.css';

class App extends Component {
  render() {
    return (
      routes.map((route, i) => {
        return (
          <Route 
              key={i}
              path={route.path}
              exact={route.exact} 
              render={(props) => <route.component {...props} key={i} />}
          />
        );
      })
    );
  }
}

export default App;
