This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Get Started

* Clone this project
* Install dependencies `npm install`
* Run in dev mode `npm run start`


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>

### `npm run build`

Builds the app for production to the `build` folder.<br>


## Structure

### Src > Pages

Here is the pages components rendered by the router 